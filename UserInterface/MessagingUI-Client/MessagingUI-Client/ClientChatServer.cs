﻿//-----------------------------------------------------------------------
// <author> 
//     M Aditya
// </author>
//
// <date> 
//     11-10-2018 
// </date>
// 
// <reviewer> 
//     
// </reviewer>
// 

// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------
namespace Masti.MessagingUIClient
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;


    using Masti.ImageProcessing;
    using Messenger;


    /// <summary>
    /// Defines the <see cref="ClientChatServer" />
    /// </summary>
    public partial class ClientChatServer : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientChatServer"/> class.
        /// </summary>
        /// 

        
        public IUXMessage messageHandler = null;
        public IImageProcessing imageHandler = null;
        //public string fromIp = GetLocalIPAddress();
        public IPAddress toIP = null;
        public IPAddress fromIP = IPAddress.Parse(GetLocalIPAddress());
        public int toPort = 0;

        public ClientChatServer()
        {
            InitializeComponent();
            FileSystemWatcher watcher = new FileSystemWatcher
            {
                Path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                NotifyFilter = NotifyFilters.LastWrite,
                Filter = "trigger.txt"
            };
            watcher.Changed += new FileSystemEventHandler(this.OnMessageReceived);
            watcher.EnableRaisingEvents = true;
            this.chatScreenTableLayout.Controls.Clear();
            this.chatScreenTableLayout.RowStyles.Clear();
            //Console.WriteLine(chatScreenTableLayout.RowCount);
            this.chatScreenTableLayout.AutoScroll = true;
            this.chatScreenTableLayout.VerticalScroll.Visible = true;
            this.chatScreenTableLayout.VerticalScroll.Maximum = this.chatScreenTableLayout.Height;
            this.chatScreenTableLayout.Padding = new Padding(0, 0, SystemInformation.VerticalScrollBarWidth, 0);

            Label name = new Label
            {
                Dock = DockStyle.Fill,

                Text = "MASTI",
                BackColor = Color.DarkSlateGray,
                ForeColor = Color.Transparent,
                Font = new Font("Arial", 35, FontStyle.Regular),
                TextAlign = ContentAlignment.MiddleCenter

            };
            logoPanel.Controls.Add(name);


        }

        /// <summary>
        /// The InvokeMessageDelegate
        /// </summary>
        /// <param name="clientName">The clientName<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public delegate void InvokeMessageDelegate(string isClientMessage, string message, string messageSentStatus);

        /// <summary>
        /// The OnMessageReceived
        /// </summary>
        /// <param name="source">The source<see cref="object"/></param>
        /// <param name="e">The e<see cref="FileSystemEventArgs"/></param>
        internal void OnMessageReceived(object source, FileSystemEventArgs e)
        {
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + "messages.txt";
            try
            {
                string[] lines = File.ReadAllLines(path);
                if (lines.Length > 1)
                {
                    this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), lines[0], lines[1], lines[2]);
                }
            }
            catch (Exception)
            {
                // Handle the exception
            }
        }

        /// <summary>
        /// The InvokeMessage
        /// </summary>
        /// <param name="IsClientMessage">The IsClientMessage<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        internal void InvokeMessage(string IsClientMessage, string message, string messageSentStatus)
        {
            if (!(string.IsNullOrEmpty(message)))
            {
                Panel row = new Panel();
                row.Dock = DockStyle.Fill;
                RichTextBox TextMessage = new RichTextBox
                {
                    Size = new Size((3 * chatScreenTableLayout.Width) / 5, 40),
                    Font = new Font("Arial", 12, FontStyle.Regular),
                    ScrollBars = RichTextBoxScrollBars.None,
                    ReadOnly = true
                };
                TextMessage.ContentsResized += new ContentsResizedEventHandler(rtb_contentResized);
                TextMessage.BorderStyle = BorderStyle.FixedSingle;
                Label time = new Label();
                string systemTime = DateTime.Now.ToString("HH:mm tt");
                Console.WriteLine("Time " + systemTime);
                DateTime dt = DateTime.Parse(systemTime);
                systemTime = dt.ToString("HH:mm");
                string date = DateTime.Now.ToString("MM/dd");
                systemTime = date + " " + systemTime;


                if (String.Equals(messageSentStatus, "true", StringComparison.Ordinal))
                {
                    time.Size = new Size(70, 10);
                    time.Text = systemTime;
                }
                else
                {
                    time.Size = new Size(120, 10);
                    time.Text = "Message not delivered";
                }
                //Console.WriteLine(system_time);
                time.Font = new Font("Arial", 8, FontStyle.Regular);

                time.TextAlign = ContentAlignment.MiddleCenter;

                if (String.Equals(IsClientMessage, "Client", StringComparison.Ordinal))
                {
                    TextMessage.SelectionColor = Color.Blue;
                    TextMessage.Dock = DockStyle.Right;
                    TextMessage.BackColor = Color.LightCyan;
                    TextMessage.AppendText(message);
                    time.Dock = DockStyle.Right;

                }
                else if (String.Equals(IsClientMessage, "Server", StringComparison.Ordinal))
                {
                    TextMessage.SelectionColor = Color.Green;
                    //textMessage.SelectionAlignment = HorizontalAlignment.Right;
                    TextMessage.Dock = DockStyle.Left;
                    //textMessage.Anchor = AnchorStyles.Right ;
                    TextMessage.BackColor = Color.Cornsilk;
                    TextMessage.AppendText(message);
                    time.Dock = DockStyle.Left;
                }

                int index = chatScreenTableLayout.RowCount++;
                this.chatScreenTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, TextMessage.Height + 10));
                chatScreenTableLayout.Controls.Add(row, 0, index - 1);
                //chatScreenTableLayout.Controls.Add(time, 0, index - 1);
                //chatScreenTableLayout.Controls.Add(textMessage, 0, index-1);            
                //this.chatScreenFlowLayout.Controls.Add(textMessage);
                row.Controls.Add(time);
                row.Controls.Add(TextMessage);
                messageTestBox.Text = string.Empty;
                //Console.WriteLine(this.chatScreenTableLayout.Height);
            }
        }
        /// <summary>
        /// The rtb_contentResized
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ContentsResizedEventArgs"/></param>
        private void rtb_contentResized(object sender, ContentsResizedEventArgs e)
        {
            ((RichTextBox)sender).Height = e.NewRectangle.Height + 5;
        }

        /// <summary>
        /// The connectButton_Click
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void connectButton_Click(object sender, EventArgs e)
        {
            //Console.WriteLine("asdsad");
            if (String.IsNullOrEmpty(clientNameTextBox.Text) || String.IsNullOrEmpty(ipAddressTextBox.Text) || String.IsNullOrEmpty(portNumberTextbox.Text))
            {
                Console.WriteLine("2");
                errorLabel.Text = "All fields are compulsory";
                errorLabel.Font = new Font("Arial", 10, FontStyle.Regular);

                return;
            }
            else
            {
                if (connectButton.Text == "Connect")
                {
                    errorLabel.Text = String.Empty;
                    clientNameTextBox.ReadOnly = true;
                    ipAddressTextBox.ReadOnly = true;
                    portNumberTextbox.ReadOnly = true;

                    toIP = IPAddress.Parse(ipAddressTextBox.Text);
                    toPort = int.Parse(portNumberTextbox.Text);
                    messageHandler = new Messager(toPort);
                    imageHandler = new ImageProcessing();

                    messageHandler.SubscribeToDataReceiver(ReceiverMessageHandler);
                    messageHandler.SubscribeToStatusReceiver(sendingStatusHandlers);

                    connectButton.Text = "Disconnect";
                }
                else
                {
                    clientNameTextBox.ReadOnly = false;
                    clientNameTextBox.Text = String.Empty;
                    ipAddressTextBox.Text = String.Empty;
                    ipAddressTextBox.ReadOnly = false;
                    portNumberTextbox.Text = String.Empty;
                    portNumberTextbox.ReadOnly = false;
                    connectButton.Text = "Connect";
                }
            }
        }

        /// <summary>
        /// The chatScreenTableLayout_ControlAdded
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="ControlEventArgs"/></param>
        private void chatScreenTableLayout_ControlAdded(object sender, ControlEventArgs e)
        {
            chatScreenTableLayout.ScrollControlIntoView(e.Control);
        }

        /// <summary>
        /// The chatScreenTableLayout_SizeChanged
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void chatScreenTableLayout_SizeChanged(object sender, EventArgs e)
        {
            foreach (Control c in chatScreenTableLayout.Controls)
            {
                foreach (Control rtb in c.Controls)
                {
                    if (rtb is RichTextBox)
                    {
                        rtb.Width = 3 * (chatScreenTableLayout.Width) / 5;
                    }

                }
            }
        }

        /// <summary>
        /// The button1_Click
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SendButton_Click(object sender, EventArgs e)
        {
            string systemTime = DateTime.Now.ToString("HH:mm tt");
            Console.WriteLine("Time " + systemTime);
            DateTime dt = DateTime.Parse(systemTime);
            systemTime = dt.ToString("HH:mm");
            string date = DateTime.Now.ToString("MM/dd");
            systemTime = date + " " + systemTime;
            messageHandler.SendMessage(messageTestBox.Text,fromIP,toIP,toPort.ToString(), systemTime);
            //InvokeMessage("Client", messageTestBox.Text, "true");
        }
        public void ReceiverMessageHandler(string message, string toIP, string fromIP, DateTime dateTime)
        {
            InvokeMessage("Server", message, "true");
        }
        public void sendingStatusHandlers(Messenger.Handler.StatusCode status, string message)
        {
            if (status.ToString() == "Success")
            {
                InvokeMessage("Client", message, "true");
            }
            else
            {
                InvokeMessage("Client", message, "false");
            }
        }
        private static string GetLocalIPAddress()
        {
            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                MessageBox.Show("No network connection available");
                return "";
            }
            var host = Dns.GetHostEntry(Dns.GetHostName());
            try
            {
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip.ToString();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return "";
        }

    }
}